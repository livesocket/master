# Bot Manual Testing Guide

To help assist in the release testing of the bot, please follow the below instructions

## All role testing

| Entry   | Result                   |
| ------- | ------------------------ |
| `!test` | Should return a response |

## Sub Only testing

## VIP Only testing

## Mod Only testing

| Entry                                                        | Result                                                      |
| ------------------------------------------------------------ | ----------------------------------------------------------- |
| `!add !test test command`                                    | Should add the command successfully                         |
| `!add !test other response`                                  | Should update the command successfully                      |
| `!add`                                                       | Should refuse to add a command without a name               |
| `!add !?% bad command`                                       | Should refuse to add command with special characters        |
| `!add some command`                                          | Should refuse to add command without !                      |
| `!add !test! test command`                                   | Should refuse to add command with extra special characters  |
| `!add !add overwrite`                                        | Should refuse to overwrite custom command                   |
| `!del !test`                                                 | Should delete the !test command                             |
| `!del test`                                                  | Should refuse to delete command name not starting with !    |
| `!del`                                                       | Should refuse to delete nothing                             |
| `!del !nothing`                                              | Should refuse to delete a command that doesn't exist        |
| `!commands`                                                  | Should return list of commands INCLUDING mod level commands |
| `!disable !test`                                             | Should disable !test                                        |
| `!disable test | Should refuse to disable command without !` |
| `!disable`                                                   | Should refuse to disable nothing                            |
| `!disable !add`                                              | Should refuse to disable custom command                     |
| `!enable !test`                                              | Should enable !test                                         |
| `!enable test`                                               | Should refuse to enable command without !                   |
| `!enable`                                                    | Should refuse to enable nothing                             |
| `!cooldown !test`                                            | Should return cooldown of 0                                 |
| `!cooldown !test 5`                                          | Should set !test cooldown to 5 seconds                      |
| `!cooldown !test hello`                                      | Should refuse to set cooldown with invalid time             |
| `!cooldown`                                                  | Should refuse to set cooldown on nothing                    |

## Broadcaster Only testing
