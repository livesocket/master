package main

import (
	"os"
	"os/exec"
	"path"
)

func docker() string {
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	return path.Join(cwd, "docker")
}

func main() {
	args := os.Args[1:]

	switch args[0] {
	case "pull":
		pull(args[1:])
		return
	case "up":
		up(args[1:])
		return
	case "down":
		down(args[1:])
		return
	case "logs":
		logs(args[1:])
		return
	}
}

func pull(args []string) {
	items := []string{"-f", "base.yml"}
	if len(args) > 0 {
		items = append(items, "-f", args[0]+".yml")
	}
	items = append(items, "pull")
	cmd := command("docker-compose", items...)
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
}

func up(args []string) {
	items := []string{"-f", "base.yml"}
	if len(args) > 0 {
		items = append(items, "-f", args[0]+".yml")
	}
	items = append(items, "up", "-d")
	cmd := command("docker-compose", items...)
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
}

func down(args []string) {
	items := []string{"-f", "base.yml"}
	if len(args) > 0 {
		items = append(items, "-f", args[0]+".yml")
	}
	items = append(items, "down")
	cmd := command("docker-compose", items...)
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
}

func logs(args []string) {
	items := []string{"-f", "base.yml"}
	if len(args) > 0 {
		items = append(items, "-f", args[0]+".yml")
	}
	items = append(items, "logs", "-f")
	cmd := command("docker-compose", items...)
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
}

func command(name string, args ...string) *exec.Cmd {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = docker()
	return cmd
}
