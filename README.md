# Livesocket Project Master Repo

## Development

To run locally use

```
docker-compose pull
docker-compose up
```

This will start the servers locally. Manual database manipulation is required to add the bot to your channel

## Testing

Run the test stack using
